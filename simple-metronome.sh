#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2023 Dimitris Kardarakos
# title="$icon_mus Metronome"

setbpm() {
	BPM="$(echo "Close Menu" | sxmo_dmenu.sh -p "Set BPM")" || return 0
	case "$BPM" in
		"Close Menu") return 0 ;;
		*) setbeats
	esac
}

setbeats() {
	BEATS_OPTIONS="
		1
		2	
		3
		4
		5
		6
		7
		8
		Close Menu
	"

	BEATS_CHOICE="$(
		echo "$BEATS_OPTIONS" |
		awk 'NF' |
		awk '{$1=$1};1' |
		sxmo_dmenu.sh -p "Select beats"
	)" || return 0

	case "$BEATS_CHOICE" in
		"Close Menu") return 0 ;;
		*) sxmo_terminal.sh python ~/.local/share/scripts/simple_metronome.py --bpm $BPM --beats $BEATS_CHOICE
	esac
}

setbpm
# sxmo_terminal.sh python ~/.local/share/python-scripts/simple_metronome.py --bpm $BPM
